<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io\Info\PathString;

use PascalEberhardProgramming\DataStore\Io\Info\PathString as This; // This package
use PascalEberhardProgramming\DataStore\Core\ValueObject as BaseValueObject;
use Symfony\Component\Filesystem;

/**
 * Directory or file path string value object
 *
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class ValueObject extends BaseValueObject\AbstractString implements BaseValueObject\StringInterface
{

    /**
     * Constructor
     *
     * @param string $path
     * @param \PascalEberhardProgramming\DataStore\Io\Info\PathString\ConfigDO|null $config
     * @throws \LogicException
     */
    public function __construct(string $path, ?This\ConfigDO $config = null)
    {
        if ('' == $path) {
            throw new \LogicException('Path must not be empty');
        }
        $this->setInput($path);

        // Init
        if (null === $config) {
            $config = new This\ConfigDO();
        }

        // Windows to Linux path?
        if ($config->windowsToLinuxConvert()) {
            $path = This\Utils::windowsToLinux($path);
        }

        // Validations

        // - Absolute or relative path?
        if ($config->checkAbsolute()) {
            // \Symfony\Component\Filesystem\Filesystem::isAbsolutePath() works without I/O operations
            $isAbsolute = (new Filesystem\Filesystem())->isAbsolutePath($path);

            if ($config->absolute() && !$isAbsolute) {
                throw new \LogicException('Path must be absolute, "' . $path . '"');
            } else if (!$config->absolute() && $isAbsolute) {
                throw new \LogicException('Path must be relative, "' . $path . '"');
            }
        }

        // Additions
        if ($config->addDirectorySeparatorAtEnd()) {
            $path = This\Utils::addDirectorySeparatorAtEnd($path);
        }

        $this->setData($path);
    }
}

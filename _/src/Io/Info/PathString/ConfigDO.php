<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io\Info\PathString;

use PascalEberhardProgramming\DataStore\DataObject;


/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see \PascalEberhardProgramming\DataStore\Io\Info\PathString\ValueObject
 */
class ConfigDO implements DataObject\BaseInterface
{

    /**
     * Must be absolute
     * If false and ::setCheckAbsoluteOrRelative(TRUE), than must be relative
     *
     * @var bool|null
     */
    private $absolute = null;

    /**
     * Add directory separator at the end
     *
     * @var bool
     */
    private $addDirectorySeparatorAtEnd = true;

    /**
     * Windows to Linux path convert?
     *
     * @var bool
     */
    private $windowsToLinuxConvert = true;

    /**
     * Must be absolute
     *
     * @return bool
     * @throws \LogicException
     * @see sefl::checkAbsolute()
     * @see sefl::setAbsolute()
     */
    public function absolute(): bool
    {
        if (null === $this->absolute) {
            throw new \LogicException('Absolute field not yet set, use ::setAbsolute()');
        }

        return $this->absolute;
    }

    /**
     * Add directory separator at the end
     *
     * @return bool
     */
    public function addDirectorySeparatorAtEnd(): bool
    {
        return $this->addDirectorySeparatorAtEnd;
    }

    /**
     * Check absolute or relative
     *
     * @return bool
     */
    public function checkAbsolute(): bool
    {
        return (null !== $this->absolute);
    }

    /**
     * Set path must be absolute or relative on FALSE
     * Only works on ::setCheckAbsoluteOrRelative(TRUE)
     *
     * @param bool|null $flag
     * @return self
     */
    public function setAbsolute(?bool $flag): self
    {
        $this->absolute = $flag;

        return $this;
    }

    /**
     * Set add directory separator at the end
     * Only works if path is directory
     *
     * @param bool $flag
     * @return self
     */
    public function setAddDirectorySeparatorAtEnd(bool $flag): self
    {
        $this->addDirectorySeparatorAtEnd = $flag;

        return $this;
    }

    /**
     * Set Windows to Linux path convert
     *
     * @param bool $flag
     * @return self
     */
    public function setWindowsToLinuxConvert(bool $flag): self
    {
        $this->windowsToLinuxConvert = $flag;

        return $this;
    }

    /**
     * Windows to Linux path convert?
     *
     * @return bool
     */
    public function windowsToLinuxConvert(): bool
    {
        return $this->windowsToLinuxConvert;
    }
}

<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io;

use PascalEberhardProgramming\DataStore\Config;
use PascalEberhardProgramming\DataStore\Io\Validation\Path\Config as PathVOConfig;
use PascalEberhardProgramming\DataStore\Io\Validation\Path\PathVO;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Utils
{

    /**
     * Default directory separator
     *
     * @var string
     */
    const DIRECTORY_SEPARATOR = '/';

    /**
     * Windows directory separator
     *
     * @var string
     */
    const DIRECTORY_SEPARATOR_WINDOWS = '\\';
//
//    /**
//     * Add directory seperator if not set
//     *
//     * @param string $path
//     * @param string $separator Optional, default: ::DIRECTORY_SEPARATOR
//     * @return string
//     * @throws \InvalidArgumentException
//     */
//    public static function dirPathAddSeperator(string $path, string $separator = self::DIRECTORY_SEPARATOR): string
//    {
//        // Parameter
//        if ('' == $separator) {
//            throw new \InvalidArgumentException('$separator must not be empty');
//        }
//
//        // Action
//        $separatorLegnth = mb_strlen($separator, Config::CHARSET);
//        if ($separator != mb_substr($path, 0 - $separatorLegnth, $separatorLegnth, Config::CHARSET)) {
//            $path .= $separator;
//        }
//
//        return $path;
//    }
//
//    /**
//     * Get directory list
//     *
//     * @param string $path
//     * @return array
//     */
//    public static function getDirList(string $path): array
//    {
//        // Find
//        $command = sprintf('find %s -type d -print | sort', escapeshellarg($path));
//        $process = new Process($command);
//var_export([
//    'command' => $process->getCommandLine(),
//]); echo PHP_EOL;
//        $process->run();
//
//        if (!$process->isSuccessful()) {
//            throw new ProcessFailedException($process);
//        }
//
//        $output = $process->getOutput();
//
//        return preg_split('/[' . "\n\r" . ']+/u', $output);
//    }
}

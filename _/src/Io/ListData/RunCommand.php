<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io\ListData;

use PascalEberhardProgramming\DataStore\Config;
use PascalEberhardProgramming\DataStore\Console\CommandAbstract;
use PascalEberhardProgramming\DataStore\Io\Utils;
use PascalEberhardProgramming\DataStore\Io\Validation\Path\Config as PathVOConfig;
use PascalEberhardProgramming\DataStore\Io\Validation\Path\PathVO;
use PHPUnit\Util\Configuration;
use Prophecy\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * List directory and file data
 * Run command
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class RunCommand extends CommandAbstract
{

    /**
     * Parameter path
     *
     * @var string
     */
    const PARAMETER_PATH = 'path';

    /**
     * Parameter type
     *
     * @var string
     */
    const PARAMETER_TYPE = 'type';

    /**
     * Parameter windows drive letter
     *
     * @var string
     */
    const PARAMETER_WINDOWS_DRIVE = 'win-drive';
//
//    /**
//     * Output data
//     *
//     * @var array
//     */
//    private $data = [];

    /**
     * Parameter path
     *
     * @var \PascalEberhardProgramming\DataStore\Io\Validation\Path\PathVO|null
     */
    protected $path = null;

    /**
     * Parameter type
     *
     * @var string
     */
    protected $type = '';

    /**
     * Parameter windows drive letter
     *
     * @var string
     */
    protected $windowsDrive = '';

    protected function configure()
    {
        $this
            ->setName('run')
            ->setDescription('List data')
            ->setHelp('List data')

            ->addOption(self::PARAMETER_PATH
                , null
                , InputOption::VALUE_OPTIONAL
                , 'Directory to search in. Use linux format, e.g.  "(/[' . self::PARAMETER_WINDOWS_DRIVE
                    . ']/)[..]/".'
                , '')
            ->addOption(self::PARAMETER_TYPE
                , null
                , InputOption::VALUE_OPTIONAL
                , 'Only directories (d), only files (f), or both ""'
                , '')
            ->addOption(self::PARAMETER_WINDOWS_DRIVE
                , null
                , InputOption::VALUE_REQUIRED
                , 'Windows drive letter. (Linux path, e.g. /c/some-dir/, is automatically replaced wiith windows path by git bash).'
                , '')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Init stuff
        $this->init($input, $output);

        // Get list
        $output->writeln(Utils::getDirList($this->path->getPath()));
//        if ('d' == $this->type) {
//            $output->writeln(Utils::getDirList($this->path->getPath()));
//        } else if ('f' == $this->type) {
//            //$output->writeln(Utils::getDirList($path));
//        }
//        // !TODO optimize
//        $path = BASEDIR . 'tmp' . DIRECTORY_SEPARATOR . 'files_input' . DIRECTORY_SEPARATOR;
//        $dirLisst = glob($path . '*', GLOB_ONLYDIR);
//        foreach ($dirLisst as $dirPath) {
//            echo PHP_EOL . '# Directory ' . basename($dirPath);
//            $dirPath .= DIRECTORY_SEPARATOR;
//            $list = glob($dirPath . '*.*');
//            $this->data = [];
//            foreach ($list as $file) {
//                $this->onFile($file);
//            }
//            $json = \json_encode($this->data, JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE);
//            file_put_contents($dirPath . 'fileInfo.json', $json);
//        }
    }

    /**
     * Custom input validation
     * Override in sub classes, if needed
     */
    protected function validateInput()
    {
        // Init
        $input = $this->input;
        
        // Mandatory parameter (Symfony console seems to ignore InputOption::VALUE_REQUIRED)
        if (!($input->hasOption(self::PARAMETER_WINDOWS_DRIVE)
            && !empty($input->getOption(self::PARAMETER_WINDOWS_DRIVE))))
        {
            throw new InvalidArgumentException('Parameter ' . self::PARAMETER_WINDOWS_DRIVE . ' must be set');
        }

        // Windows drive letter
        $this->windowsDrive = strtolower($input->getOption(self::PARAMETER_WINDOWS_DRIVE));

        if (!ctype_alpha($this->windowsDrive)) {
            throw new InvalidArgumentException('Parameter ' . self::PARAMETER_WINDOWS_DRIVE . ' must be a letter');
        }

        // Path
        $path = '/' . $this->windowsDrive . '/';

        if ($input->hasOption(self::PARAMETER_PATH)
            && !empty($input->getOption(self::PARAMETER_PATH)))
        {
            $path .= $input->getOption(self::PARAMETER_PATH);
        }

        $this->path = new PathVO($path, (new PathVOConfig())
            ->setMustExist(true)
            ->setFile(false)
        );

        // Type
        if ($input->hasOption(self::PARAMETER_TYPE)
            && !empty($input->getOption(self::PARAMETER_TYPE)))
        {
            $this->type = mb_strtolower($input->getOption(self::PARAMETER_TYPE));
        }

        if ('' != $this->type && 'd' != $this->type && 'f' != $this->type) {
            throw new InvalidArgumentException('Parameter ' . self::PARAMETER_TYPE . ' must be "d" or "f"');
        }

    }
//
//    /**
//     * Get file extra data, e.g. video data
//     *
//     * @param string $path File path
//     * @param string $mimeType
//     * @return array
//     */
//    private function extraDataByMimeType(string $path, string $mimeType): array
//    {
//        $data = [];
//        if ('' == $mimeType) {
//            return $data;
//        }
//        $mimeType = mb_strtolower($mimeType, Config::CHARSET);
//
//        $len = mb_strlen($mimeType, Config::CHARSET);
//        if ($len >= 6) {
//            switch (mb_substr($mimeType, 0, 6, Config::CHARSET)) {
//                case 'video/':
//                    $process = new Process('C:\bin\ffmpeg\bin\ffprobe.exe "' . $path . '" 2>&1');
//                    $process->run();
//
//                    if (!$process->isSuccessful()) {
//                        throw new ProcessFailedException($process);
//                    }
//
//                    $data = preg_split('/[' . "\n\r" . ']+/u', $process->getOutput());
//                    break;
//            }
//        }
//
//        return $data;
//    }
//
//    /**
//     * Get file info
//     *
//     * @param string $path File path
//     */
//    private function onFile(string $path)
//    {
//        if (!is_file($path)) {
//            return;
//        }
//        $data = [
//            'data' => [],
//            'extra' => [],
//            'hash' => [],
//            'io' => [],
//        ];
//        $pathLen = mb_strlen($path, Config::CHARSET);
//
//        // I/O data
//        $info = pathinfo($path);
//        $size = filesize($path);
//        $data['io'] = [
//            'ext' => $info['extension'],
//            'name' => $info['filename'], // PHP 5.2.0
//            'size' => $size,
//        ];
//        echo PHP_EOL . '## ' . $info['basename'];
//
//        // Linux:file data
//        $process = new Process('file -i "' . $path . '"');
//        $process->run();
//
//        if (!$process->isSuccessful()) {
//            throw new ProcessFailedException($process);
//        }
//
//        $res = mb_substr($process->getOutput(), $pathLen, null, Config::CHARSET);
//        $res = preg_split('/[:\s;]+/iu', $res);
//        $res = array_unique($res);
//
//        $firstLine = false;
//        $item = [
//            'charset' => '',
//            'extra' => [],
//            'mimeType' => '',
//        ];
//        foreach ($res as $line) {
//            if ('' == $line) {
//                continue;
//            }
//
//            if ('' == $item['charset'] && 'charset=' == mb_substr($line, 0, 8, Config::CHARSET)) {
//                $item['charset'] = $line;
//                continue;
//            }
//
//            if (!$firstLine) {
//                $firstLine = false;
//                $item['mimeType'] = $line;
//                continue;
//            }
//
//            $item['extra'][] = $line;
//        }
//
//        $data['data'] = $item;
//
//        // Content hash
//        $process = new Process('sha512sum "' . $path . '"');
//        $process->run();
//
//        if (!$process->isSuccessful()) {
//            throw new ProcessFailedException($process);
//        }
//
//        $res = mb_substr($process->getOutput(), 1, null, Config::CHARSET);
//        $pos = mb_strpos($res, ' ', 0, Config::CHARSET);
//        if (false !== $pos) {
//            $res = mb_substr($res, 0, $pos, Config::CHARSET);
//        }
//        $data['hash'] = [
//            'sha512' => $res,
//        ];
//
//        // Extra data
//        $data['extra'] = $this->extraDataByMimeType($path, $data['data']['mimeType']);
//
//        $this->data[] = $data;
///*
//        "extra": [
//            "ffprobe version N-79209-gb3eda69 Copyright (c) 2007-2016 the FFmpeg developers",
//            "built with gcc 5.3.0 (GCC)",
//            "configuration: --enable-gpl --enable-version3 --disable-w32threads --enable-avisynth --enable-bzlib --enable-fontconfig --enable-frei0r --enable-gnutls --enable-iconv --enable-libass --enable-libbluray --enable-libbs2b --enable-libcaca --enable-libfreetype --enable-libgme --enable-libgsm --enable-libilbc --enable-libmodplug --enable-libmfx --enable-libmp3lame --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libopenjpeg --enable-libopus --enable-librtmp --enable-libschroedinger --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libtheora --enable-libtwolame --enable-libvidstab --enable-libvo-amrwbenc --enable-libvorbis --enable-libvpx --enable-libwavpack --enable-libwebp --enable-libx264 --enable-libx265 --enable-libxavs --enable-libxvid --enable-libzimg --enable-lzma --enable-decklink --enable-zlib",
//            "libavutil      55. 19.100 / 55. 19.100",
//            "libavcodec     57. 33.100 / 57. 33.100",
//            "libavformat    57. 29.101 / 57. 29.101",
//            "libavdevice    57.  0.101 / 57.  0.101",
//            "libavfilter     6. 40.102 /  6. 40.102",
//            "libswscale      4.  1.100 /  4.  1.100",
//            "libswresample   2.  0.101 /  2.  0.101",
//            "libpostproc    54.  0.100 / 54.  0.100",
//            "Input #0, mpeg, from '<path>':",
//            "Duration: 00:05:42.44, start: 0.224400, bitrate: 3186 kb/s",
//            "Stream #0:0[0x1e0]: Video: mpeg2video (Main), yuv420p(tv), 720x576 [SAR 64:45 DAR 16:9], max. 3000 kb/s, 25 fps, 25 tbr, 90k tbn, 50 tbc",
//            "Stream #0:1[0x80]: Audio: ac3, 48000 Hz, stereo, fltp, 192 kb/s"
//        ],
//]*/
//    }
}

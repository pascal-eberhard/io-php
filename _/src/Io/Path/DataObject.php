<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io\Info\PathString;

use PascalEberhardProgramming\DataStore\DataObject as BaseDataObject;
use PascalEberhardProgramming\DataStore\Io\Info\PathString as This; // This package
use Symfony\Component\Filesystem\Filesystem;

/**
 * Directory or file path string data object
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class DataObject implements BaseDataObject\BaseInterface
{
    // !TODO
}

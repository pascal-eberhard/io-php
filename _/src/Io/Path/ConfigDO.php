<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io\Path;

use PascalEberhardProgramming\DataStore\DataObject;
use PascalEberhardProgramming\DataStore\Io\Info\PathString;


/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see \PascalEberhardProgramming\DataStore\Io\Info\PathString\ValueObject
 */
class ConfigDO extends PathString\ConfigDO implements DataObject\BaseInterface
{

    /**
     * Fill path string config data object
     *
     * @todo Via dependency injection and class paths
     * @param \PascalEberhardProgramming\DataStore\Io\Info\PathString\ConfigDO|null $config Optional
     * @return \PascalEberhardProgramming\DataStore\Io\Info\PathString\ConfigDO
     */
    public function fillPathStringConfig(?PathString\ConfigDO $config = null): PathString\ConfigDO
    {
        if (null === $config) {
            $config = new PathString\ConfigDO();
        }

        $config->setAbsolute(null);
        $config->setAddDirectorySeparatorAtEnd($this->addDirectorySeparatorAtEnd());
        $config->setWindowsToLinuxConvert($this->windowsToLinuxConvert());

        if ($this->checkAbsolute()) {
            $config->setAbsolute($this->absolute());
        }

        return $config;
    }
//
//    /**
//     * Check directory or file
//     *
//     * @var bool
//     */
//    private $checkDirectoryOrFile = false;
//
//    /**
//     * Must be file
//     * If false and ::setCheckDirectoryOrFile(TRUE), than must be directory
//     *
//     * @var bool
//     */
//    private $file = false;
//
//    /**
//     * Must exist
//     * If TRUE, path must be absolute
//     *
//     * @var bool
//     */
//    private $mustExist = false;
//
//    /**
//     * Path with backslash?
//     *
//     * @var bool
//     */
//    protected $withBackslash = false;
//
//    /**
//     * Must be absolute
//     *
//     * @return bool
//     */
//    public function absolute(): bool
//    {
//        return $this->absolute;
//    }
//
//    /**
//     * Add tailing directory separator
//     *
//     * @return bool
//     */
//    public function addDirectorySeparatorAtEnd(): bool
//    {
//        return $this->addDirectorySeparatorAtEnd;
//    }
//
//    /**
//     * Check absolute or relative
//     *
//     * @return bool
//     */
//    public function checkAbsoluteOrRelative(): bool
//    {
//        return $this->checkAbsoluteOrRelative;
//    }
//
//    /**
//     * Check directory or file
//     *
//     * @return bool
//     */
//    public function checkDireectoryOrFile(): bool
//    {
//        return $this->checkDirectoryOrFile;
//    }
//
//    /**
//     * Must be file
//     *
//     * @return bool
//     */
//    public function file(): bool
//    {
//        return $this->file;
//    }
//
//    /**
//     * Must exist
//     *
//     * @return bool
//     */
//    public function mustExist(): bool
//    {
//        return $this->mustExist;
//    }
//
//    /**
//     * Set path must be absolute or relative on FALSE
//     * Only works on ::setCheckAbsoluteOrRelative(TRUE)
//     *
//     * @param bool $flag
//     * @return self
//     */
//    public function setAbsolute(bool $flag): self
//    {
//        $this->absolute = $flag;
//        return $this;
//    }
//
//    /**
//     * Set add tailing directory separator
//     * Only works if path is directory
//     *
//     * @param bool $flag
//     * @return self
//     */
//    public function setaddDirectorySeparatorAtEnd(bool $flag): self
//    {
//        $this->addDirectorySeparatorAtEnd = $flag;
//        return $this;
//    }
//
//    /**
//     * Set check absolute or relative
//     *
//     * @param bool $flag
//     * @return self
//     */
//    public function setCheckAbsoluteOrRelative(bool $flag): self
//    {
//        $this->checkAbsoluteOrRelative = $flag;
//        return $this;
//    }
//
//    /**
//     * Set check directory or file
//     *
//     * @param bool $flag
//     * @return self
//     */
//    public function setCheckDirectoryOrFile(bool $flag): self
//    {
//        $this->checkDirectoryOrFile = $flag;
//        return $this;
//    }
//
//    /**
//     * Set path must be directory (FALSE) or file (TRUE)
//     * Only works on ::setCheckDirectoryOrFile(TRUE)
//     *
//     * @param bool $flag
//     * @return self
//     */
//    public function setDirectory(bool $flag): self
//    {
//        $this->file = !$flag;
//        return $this;
//    }
//
//    /**
//     * Set path must be directory (FALSE) or file (TRUE)
//     * Only works on ::setCheckDirectoryOrFile(TRUE)
//     *
//     * @param bool $flag
//     * @return self
//     */
//    public function setFile(bool $flag): self
//    {
//        $this->file = $flag;
//        return $this;
//    }
//
//    /**
//     * Set must exist
//     *
//     * @param bool $flag
//     * @return self
//     */
//    public function setMustExist(bool $flag): self
//    {
//        $this->mustExist = $flag;
//
//        if ($flag) {
//            $this->absolute = true;
//            $this->checkAbsoluteOrRelative = true;
//            $this->checkDirectoryOrFile = true;
//        }
//
//        return $this;
//    }
//
//    /**
//     * Set path with backslash
//     *
//     * @param bool $flag
//     * @return self
//     */
//    public function setWithBackslash(bool $flag): self
//    {
//        $this->withBackslash = $flag;
//        return $this;
//    }
//
//    /**
//     * Path with backslash
//     *
//     * @return bool
//     */
//    public function withBackslash(): bool
//    {
//        return $this->withBackslash;
//    }
}

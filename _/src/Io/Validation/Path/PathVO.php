<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io\Validation\Path;

use PascalEberhardProgramming\DataStore\Config as ProjectConfig;
use PascalEberhardProgramming\DataStore\Io\Utils;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Directory or file path value object
 *
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class PathVO
{

    /**
     * Correct backslashes
     *
     * @param string $path
     * @return string
     */
    public static function backslashCorrection(string $path): string
    {
        if ('' == $path) {
            return '';
        }

        $path = preg_replace('/' . "\n" . '/iu', '/n', $path);
        $path = preg_replace('/' . "\r" . '/iu', '/r', $path);
        $path = preg_replace('/' . "\t" . '/iu', '/t', $path);
        $path = str_replace('\\', '/', $path);

        return $path;
    }

    /**
     * Path to windows format
     *
     * @param string $path
     * @return string
     */
    public static function toWindowsFormat(string $path): string
    {
        // Validations
        if ('' == $path) {
            return '';
        }

        return str_replace('/', Utils::DIRECTORY_SEPARATOR_WINDOWS, $path);
    }

    /**
     * Config
     *
     * @var \PascalEberhardProgramming\DataStore\Io\Validation\Path\Config
     */
    private $config = null;

    /**
     * Path
     *
     * @var string
     */
    private $path = '';

    /**
     * Windows path
     *
     * @var string
     */
    private $pathWindows = '';

    /**
     * Constructor
     *
     * @param string $path
     * @param \PascalEberhardProgramming\DataStore\Io\Validation\Path\Config|null $config
     * @throws \LogicException
     */
    public function __construct(string $path, $config = null)
    {
        // Iniz
        if (null === $config) {
            $config = new Config();
        }

        // Validations
        if ('' == $path) {
            throw new \LogicException('Path must not be empty');
        }

        // Backslash correction
        if ($config->withBackslash()) {
            $path = self::backslashCorrection($path);
        }

        // - Absolute or relative path?
        if ($config->checkAbsoluteOrRelative()) {
            $fileSystem = new Filesystem();
            $isAbsolute = $fileSystem->isAbsolutePath($path);

            if ($config->absolute() && !$isAbsolute) {
                throw new \LogicException('Path must be absolute, "' . $path . '"');
            } else if (!$config->absolute() && $isAbsolute) {
                throw new \LogicException('Path must be relative, "' . $path . '"');
            }
        }

        // - Create windows format
        $this->path = $path;
        $this->pathWindows = self::toWindowsFormat($path);
        $usePath = $path;

        if ('windows' == strtolower(PHP_OS_FAMILY)) {
            $usePath = $this->pathWindows;
        }

        // - Exist?
        if ($config->mustExist()) {

            if ($config->file() && !is_file($usePath)) {
                throw new \LogicException('Path is no file, "' . $usePath . '"');
            } else if (!$config->file() && !is_dir($usePath)) {
                throw new \LogicException('Path is no directory, "' . $usePath . '"');
            }
        }

        // Additions
        if ($config->addDirectorySeparatorAtEnd() && !$config->file()) {
            $this->path = Utils::dirPathAddSeperator($this->path);
            $this->pathWindows = Utils::dirPathAddSeperator($this->pathWindows,
                Utils::DIRECTORY_SEPARATOR_WINDOWS);
        }
    }

    /**
     * Path
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Windows format path
     *
     * @return string
     */
    public function getPathWindowsFormat(): string
    {
        return $this->pathWindows;
    }
}

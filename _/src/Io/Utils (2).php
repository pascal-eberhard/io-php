<?php
declare(strict_types=1);

namespace my_project\io;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Utils for I/O stuff
 *
 * @author Pascal Eberhard
 */
class Utils
{

    /**
     * Get image data
     *
     * @param string $path
     * @param bool $outputQuality Optional, default: FALSE
     * @return array ["quality": imt, "x": int, "y": int]
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @throws \Symfony\Component\Process\Exception\ProcessFailedException
     * @throws \UnexpectedValueException
     */
    public static function getImageData(string $path, bool $outputQuality = false): array
    {
        if (!('' != $path && is_file($path))) {
            throw new \InvalidArgumentException('$path must be path to image file');
        }

        $format = '{\'x\':\'%w\',\'y\':\'%h\'}';
        if ($outputQuality) {
            $format = '{\'quality\':\'%Q\',\'x\':\'%w\',\'y\':\'%h\'}';
        }

        $p = new Process('identify -format "' . $format . '" -quiet '
            . sprintf('%s', escapeshellarg($path)));

        $p->run();
        if (!$p->isSuccessful()) {
            throw new ProcessFailedException($p);
        }

        $res = $p->getOutput();
        if ('' === $res) {
            throw new \RuntimeException('Empty result');
        }

        $res = json_decode(str_replace('\'', '"', $res), true);
        if (!(ctype_digit($res['x']) && ctype_digit($res['y']))) {
            throw new \UnexpectedValueException('All result item values should be numeric');
        } else if ($outputQuality && !ctype_digit($res['quality'])) {
            throw new \UnexpectedValueException('All result item values should be numeric');
        }

        $res['x'] = intval($res['x']);
        $res['y'] = intval($res['y']);
        if ($outputQuality) {
            $res['quality'] = intval($res['quality']);
        }

        return $res;
    }

    /**
     * Path to Linux format ("\\" to "/")
     *
     * @param string $path
     * @return string
     */
    public static function pathToLinuxFormat(string $path): string
    {
        if ('' == $path) {
            return '';
        }

        // str_replace() is not multi-byte safe, but preg_replace has problems with "\" in path, why ever
        return str_replace('\\', '/', $path);
    }

    /**
     * Path to Windows format ("/" to "\\")
     *
     * @param string $path
     * @return string
     */
    public static function pathToWindowsFormat(string $path): string
    {
        if ('' == $path) {
            return '';
        }

        // str_replace() is not multi-byte safe, but preg_replace has problems with "\" in path, why ever
        return str_replace('/', '\\', $path);
    }
}

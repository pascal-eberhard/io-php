<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io\Info\Command;

use MyCLabs\Enum;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class PathListType extends Enum\Enum
{

    /**
     * Directory / folder
     *
     * @var string
     */
    const DIRECTORY = 'd';

    /**
     * File
     *
     * @var string
     */
    const FILE = 'f';
}

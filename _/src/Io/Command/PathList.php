<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\Io\Info\Command;

use PascalEberhardProgramming\DataStore\Config;
use PascalEberhardProgramming\DataStore\Console;
use PascalEberhardProgramming\DataStore\IoInfo;
use Prophecy\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Yaml\Yaml;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class PathList extends Console\CommandAbstract
{

    /**
     * Command name
     *
     * @var string
     */
    const COMMAND = 'pathList';

    /**
     * Parameter path
     *
     * @var string
     */
    const PARAMETER_PATH = 'path';

    /**
     * Parameter type
     *
     * @var string
     */
    const PARAMETER_TYPE = 'type';

    /**
     * I/O data
     *
     * @var array
     */
    private $data = [];

    /**
     * Parameter path
     *
     * @var string
     *  @ var \PascalEberhardProgramming\DataStore\Io\Validation\Path\PathVO|null
     */
    protected $path = '';

    protected function configure()
    {
        $this
            ->setName(self::COMMAND)
            ->setDescription('Get files or folder path list.')

            ->addArgument(self::PARAMETER_PATH
                , InputArgument::REQUIRED
                , 'Directory to search in. Use linux format, e.g.  "(/c/)[..]/".'
            )

            ->addOption(self::PARAMETER_TYPE
                , null
                , InputOption::VALUE_IS_ARRAY + InputOption::VALUE_REQUIRED
                , 'Search directories (' . PathListType::DIRECTORY . ')'
                    . ', files (' . PathListType::FILE . ').'
                , [PathListType::DIRECTORY, PathListType::FILE]
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Init stuff
        $this->init($input, $output);

        $this->getData();

        $output->writeln([
            '(YAML)',
            '# // !TODO complete',
        ]);

        $output->writeln(Yaml::dump([
            'folder' => $this->data,
        ], 2));

        $output->writeln('(/YAML)');
    }

    /**
     * Get I/O data
     */
    protected function getData()
    {
//        $p = new Process('find "' . $this->path . '"'
//            . ' -type d'
//            . ' -not -ipath "*/*recycle*"'
//            . ' -not -ipath "*/system*volume*"'
//            . ' -print'
//            . ' | sort'
//            . ' 2>&1'
//        );
//
//        $this->data = [];
//        $p->run();
//
//        if (!$p->isSuccessful()) {
//            throw new \LogicException($p->getExitCodeText());
//        }
//
//        $this->data = preg_split('/[' . "\n\r" . ']+/iu', $p->getOutput());
    }

    /**
     * Custom init stuff
     * Override in sub classes, if needed
     */
    protected function onInit()
    {
        $this->output->writeln([
            '(YAML)',
            '# Shell input',
        ]);

        $this->path = $this->input->getArgument(self::PARAMETER_PATH);
        $this->output->writeln(Yaml::dump([
            'arguments' => $this->input->getArguments(),
            'options' => $this->input->getOptions(),
            'path-info' => IoInfo\PathString\Utils::pathWindowsToLinux($this->path),
        ], 2));

        $this->output->writeln('(/YAML)');
        // !TODO via config
        $this->path = $this->input->getArgument(self::PARAMETER_PATH);
//        $this->path = '/c/_buffer/_f';
    }
}

<?php
declare(strict_types=1);

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\FileInfo;

use PascalEberhardProgramming\DataStore\Config;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Utils
{

    /**
     * Parse file hash data
     * Format: Linux shell
     *
     * @param string $data
     * @return array
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * Shell: (find . -type f -exec md5sum "{}" + | sort)
     * Shell: (find . -type f -exec sha1sum "{}" + | sort)
     */
    public static function parseFileHashDataLinux(string $data): array
    {
        if ('' === $data) {
            return [];
        }

        if (0 === preg_match_all('/^\s*([0-9a-f]+)\s+\*\s*([^' . "\n\r" . ']+)[' . "\n\r" . '\s]*$/imu', $data,
                $match)
        ) {
            throw new \LogicException('No hash items found');
        }

        if (!(isset($match[1])
            && isset($match[2])
            && is_array($match[1])
            && is_array($match[2])
            && count($match[1]) === count($match[2])
        )) {
            throw new \UnexpectedValueException('Unexpected format');
        }

        $hashes = $match[1];
        $paths = $match[2];
        $result = [];

        foreach ($hashes as $index => $hash) {
            $result[] = [
                'hash' => $hash,
                'path' => $paths[$index],
            ];
        }

        return $result;
    }

    /**
     * Parse file size data
     * Format: Linux shell
     *
     * @param string $data
     * @return array
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * Shell: (find . -type f -exec ls -glnR --full-time "{}" + | sort)
     */
    public static function parseFileSizeDataLinux(string $data): array
    {
        if ('' === $data) {
            return [];
        }

        #                                   'rw-r--r-- 1 197121   52168 2018-09-01 23:05:32.105240700 +0200 /c/_buffer/zzz/markdown.html'
        if (0 === preg_match_all('/^\s*[\-drw]+\s+\d+\s+\d+\s+(\d+)\s+.*\+\d{4}\s+'
                . '([^' . "\n\r" . ']+)[' . "\n\r" . '\s]*$/imu', $data, $match)
        ) {
            throw new \LogicException('No size items found');
        }

        if (!(isset($match[1])
            && isset($match[2])
            && is_array($match[1])
            && is_array($match[2])
            && count($match[1]) === count($match[2])
        )) {
            throw new \UnexpectedValueException('Unexpected format');
        }

        $paths = $match[2];
        $result = [];
        $sizes = $match[1];

        foreach ($sizes as $index => $size) {
            $result[] = [
                'path' => $paths[$index],
                'size' => intval($size),
            ];
        }

        return $result;
    }

    /**
     * Parse paths
     * Format: Linux shell
     *
     * @param string $data
     * @return array
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * Shell: (find . -type d | sort)
     * Shell: (find . -type f | sort)
     */
    public static function parsePathsLinux(string $data): array
    {
        if ('' === $data) {
            return [];
        }

        if (0 === preg_match_all('/^\s*([^' . "\n\r" . ']+)[' . "\n\r" . '\s]*$/imu', $data,
                $match)
        ) {
            throw new \LogicException('No path items found');
        }

        if (!(isset($match[1]) && is_array($match[1]))) {
            throw new \UnexpectedValueException('Unexpected format');
        }

        return array_unique($match[1]);
    }
}

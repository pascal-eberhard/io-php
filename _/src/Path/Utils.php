<?php
declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Io\Path;

use PEPrograms\Io\MyConfig as Conf;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2018 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Utils
{

    /**
     * Get directory name
     * PHP::dirname() converts linux directory separators to windows ones, at windows operating system
     *
     * @param string $path
     * @param int $level Optional, default: 1
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function dirname(string $path, int $level = 1): string
    {
        if (!Conf::operatingSystemIsWindows()) {
            return \dirname($path, $level);
        }

        // @todo make equal to PHP:\dirname()
        // - Empty path
        // - $level smaller as 1
        // - $path contains slash and back slash

        print PHP_EOL;
        var_export([
            '$path.original' => $path,
        ]);
        print PHP_EOL;

        $path = self::dirnameStep($path);

#        if ()
        #for ($i =)

        print PHP_EOL;
        var_export([
            '$path.1' => $path,
        ]);
        print PHP_EOL;

        return $path;
    }

    /**
     * Get directory name, step
     *
     * @param string $path
     * @return string
     * @see self::dirname()
     */
    protected static function dirnameStep(string $path): string
    {
        if ('' == $path) {
            return $path;
        }

        // Search next directory separator from string end
        $posBackSlash = \mb_strrpos($path, '\\', 0, Conf::CHARSET_UTF8);
        $posSlash = \mb_strrpos($path, '/', 0, Conf::CHARSET_UTF8);
        if (false === $posBackSlash && false === $posSlash) {
            return $path;
        }

        $length = \mb_strlen($path, Conf::CHARSET_UTF8);


        $lastChar = \mb_substr($path, -1, 1, Conf::CHARSET_UTF8);
        if ()
        if (!Conf::operatingSystemIsWindows()) {
            return \dirname($path, $level);
        }

        print PHP_EOL;
        var_export([
            '$path.original' => $path,
        ]);
        print PHP_EOL;

        $path = self::dirnameStep($path);

        $posBackSlash = \mb_strrpos($path, '\\', 0, Conf::CHARSET_UTF8);
        $posSlash = \mb_strrpos($path, '/', 0, Conf::CHARSET_UTF8);
        if (false === $posBackSlash && false === $posSlash) {
            return $path;
        }
#        if ()
        #for ($i =)

        print PHP_EOL;
        var_export([
            '$path' => $path,
            '$posBackSlash' => $posBackSlash,
            '$posSlash' => $posSlash,
        ]);
        print PHP_EOL;

        return \dirname($path, $level);
    }
}

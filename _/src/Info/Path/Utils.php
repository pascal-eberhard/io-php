<?php
declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Io\Info\Path;

use PEPrograms\Io\MyConfig as Conf;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @copyright 2018 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
class Utils
{

    /**
     * Add tailing directory separator to path, if missing
     *
     * @param string $path
     * @param string $separator Optional, the directory separator default: Conf::DIRECTORY_SEPARATOR
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function addTailingSeparator(string $path, string $separator = Conf::DIRECTORY_SEPARATOR): string
    {
        if ('' == $separator) {
            throw new \InvalidArgumentException('$separator must not be empty');
        }

        if ('' == $path) {
            return $separator;
        }

        $separatorLength = \mb_strlen($separator, Conf::CHARSET_UTF8);
        if ($separator != \mb_substr($path, 0 - $separatorLength, $separatorLength, Conf::CHARSET_UTF8)) {
            return $path . $separator;
        }

        return $path;
    }

    /**
     * Is windows path?
     *
     * @param string $path
     * @return bool
     */
    public static function isWindowsFormat(string $path): bool
    {
        if ('' == $path) {
            return false;
        }

        $fistChar = \mb_substr($path, 0, 1, Conf::CHARSET_UTF8);
        if ('/' == $fistChar) {
            return false;
        } elseif (!\ctype_alpha($fistChar)) {
            return false;
        } elseif (0 === \preg_match('/^[a-z]+:(\\\\|\/)/iu', $path)) {
            return false;
        }

        return true;
    }

    /**
     * Convert linux to windows path
     *
     * @param string $path
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function linuxToWindows(string $path): string
    {
        if ('' == $path) {
            throw new \InvalidArgumentException('$path must not be empty');
        }

        // We need no convert, if $path not contains slash
        if (false === \mb_strpos($path, '/', 0, Conf::CHARSET_UTF8)) {
            return $path;
        }

        // Convert (there is no real mb_str_replace(), so use preg_replace in Unicode mode)
        $path = self::linuxToWindowsConvertPrefix($path);
        $path = \preg_replace('/\//iu', '\\', $path);

        return $path;
    }

    /**
     * Convert linux to windows path, convert prefix to windows drive letter, if possible
     *
     * @param string $path
     * @return string
     */
    protected static function linuxToWindowsConvertPrefix(string $path): string
    {
        // Requirements:
        // - $path has 3 char prefix
        // - prefix starts and ends with "/"
        // - prefix middle is letter
        if ('' == $path) {
            return $path;
        } elseif (\mb_strlen($path, Conf::CHARSET_UTF8) < 3) {
            return $path;
        } elseif ('/' != \mb_substr($path, 0, 1, Conf::CHARSET_UTF8)) {
            return $path;
        } elseif ('/' != \mb_substr($path, 2, 1, Conf::CHARSET_UTF8)) {
            return $path;
        }

        $letter = \mb_substr($path, 1, 1, Conf::CHARSET_UTF8);
        if (!\ctype_alpha($letter)) {
            return $path;
        }

        $letter = \mb_strtoupper($letter, Conf::CHARSET_UTF8);

        return $letter . ':' . \mb_substr($path, 2, null, Conf::CHARSET_UTF8);
    }

    /**
     * Split path
     *
     * @param string $path
     * @return string[] List of path parts and separators
     */
    public static function split(string $path): array
    {
        if ('' == $path) {
            return [];
        }

        print PHP_EOL; var_export([
            '$path' => $path,
        ]); print PHP_EOL;

        $do = true;
        $lastPart = '';
        $parts = [];
        while ($do) {
            $part = \dirname($path);
            if ($lastPart == $part) {
                $do = false;
                break;
            }
            print PHP_EOL; var_export([
                'basename' => \basename($path),
                'dirname' => $part,
            ]); print PHP_EOL;

            $lastPart = $part;
            $parts[] = $part;
            $path = $part;
        }
//        do {
//
//            if
//            $parts[] = [
//            ];
//        } while (false);
//        for ($i = 0; $i < 8; ++$i) {
//            array_unshift($parts, [
//                'basename' => basename($path),
//                'dirname' => dirname($path),
//                'dirname.2' => dirname($path, 2),
//            ]);
//            $path = dirname($path);
//        }
////        do {
////            $parts[] = basename($path);
////            $path = dirname($path);
////
////            #$path = '';
////        } while ('' != $path);

        # DEBUG
        print PHP_EOL; var_export($parts); print PHP_EOL;

        return $parts;
    }

    /**
     * Convert windows to linux path
     *
     * @param string $path
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function windowsToLinux(string $path): string
    {
        if ('' == $path) {
            throw new \InvalidArgumentException('$path must not be empty');
        }

        // We need no convert, if $path not contains backslash
        if (false === \mb_strpos($path, '\\', 0, Conf::CHARSET_UTF8)) {
            return $path;
        }

        // Convert (there is no real mb_str_replace(), so use preg_replace in Unicode mode)
        $path = \preg_replace('/\\\\/iu', '/', $path);
        if (':/' == \mb_substr($path, 1, 2, Conf::CHARSET_UTF8)) {
            // Convert Windows path prefix
            $path = \preg_replace_callback('/^([a-z]):\//iu', function ($match) {
                return '/' . \mb_strtolower($match[1], Conf::CHARSET_UTF8) . '/';
            }, $path);
        }

        return $path;
    }
}

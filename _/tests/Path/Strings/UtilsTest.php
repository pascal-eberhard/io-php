<?php declare(strict_types=1);

/*
 * This file is part of the typed-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ConfigSimple\Tests\External\Io\Path\Strings;

use PEPrograms\ConfigSimple\External\Io\Path\Strings\Data;
use PEPrograms\ConfigSimple\External\Io\Path\Strings\DataObjectInterface;
use PEPrograms\ConfigSimple\External\Io\Path\Strings\Utils;
use PEPrograms\ConfigSimple\External\UnitTests\WithServiceTrait;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\ConfigSimple\External\Io\Path\Strings\Utils
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/UtilsTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class UtilsTest extends TestCase
{

    use WithServiceTrait;

    /**
     * Create service instance, different per sub class
     *
     * @return object
     */
    protected function createService()
    {
        return Utils::get();
    }

    /**
     * @return array, per item:
     * param string[] $expected See DataObjectInterface::toArray()
     * param string $path Empty allowed
     * param string $separator Directory separator, default: DIRECTORY_SEPARATOR
     * @see self::testDefault()
     * @see \PEPrograms\ConfigSimple\External\Io\Path\Strings\DataObjectInterface::toArray()
     */
    public function dataDefault()
    {
        $return = [];
//
//        // -
//        $path = '';
//        $return[] = [
//            (new Data())->toArray(),
//            $path,
//            '/',
//        ];
//
//        // -
//        $path = 'fileName.txt';
//        $return[] = [
//            [
//                'path' => $path,
//                'pathParts' => [
//                    'fileName.txt',
//                ],
//                'pathPartsCount' => 1,
//                'separators' => [
//                ],
//                'separatorsCount' => 0,
//            ],
//            $path,
//            '/',
//        ];
//
//        // -
//        $path = '/someAbsolute/linux/filePath.txt';
//        $return[] = [
//            [
//                'path' => $path,
//                'pathParts' => [
//                    'someAbsolute',
//                    'linux',
//                    'filePath.txt',
//                ],
//                'pathPartsCount' => 3,
//                'separators' => [
//                    '/',
//                    '/',
//                    '/',
//                ],
//                'separatorsCount' => 3,
//            ],
//            $path,
//            '/',
//        ];
//
//        // -
//        $path = 'someRelative/linux/filePath.txt';
//        $return[] = [
//            [
//                'path' => $path,
//                'pathParts' => [
//                    'someRelative',
//                    'linux',
//                    'filePath.txt',
//                ],
//                'pathPartsCount' => 3,
//                'separators' => [
//                    '/',
//                    '/',
//                ],
//                'separatorsCount' => 2,
//            ],
//            $path,
//            '/',
//        ];
//
//        // -
//        $path = 'C:\\someAbsolute\\Windows\\filePath.txt';
//        $return[] = [
//            [
//                'path' => $path,
//                'pathParts' => [
//                    'C:',
//                    'someAbsolute',
//                    'Windows',
//                    'filePath.txt',
//                ],
//                'pathPartsCount' => 4,
//                'separators' => [
//                    '\\',
//                    '\\',
//                    '\\',
//                ],
//                'separatorsCount' => 3,
//            ],
//            $path,
//            '\\',
//        ];

        // -
        $path = 'someRelative\\Windows\\filePath.txt';
        $return[] = [
            [
                'path' => $path,
                'pathParts' => [
                    'someRelative',
                    'Windows',
                    'filePath.txt',
                ],
                'pathPartsCount' => 3,
                'separators' => [
                    '\\',
                    '\\',
                ],
                'separatorsCount' => 2,
            ],
            $path,
            '\\',
        ];

        // -
        return $return;
    }
    /**
     * @covers ::split
     * @dataProvider dataDefault
     *
     * @param string[] $expected See DataObjectInterface::toArray()
     * @param string $path Empty allowed
     * @param string $separator Directory separator, default: DIRECTORY_SEPARATOR
     *
     * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/UtilsTest.php --filter '/::testDefault\b/')
     * @ \b, else all tests matching "testX*" would be executed
     * @see \PEPrograms\ConfigSimple\External\Io\Path\Strings\DataObjectInterface::toArray()
     */
    public function testDefault(array $expected, string $path, string $separator)
    {
        (new DataTest())->testFilled($expected, $this->getService()->split($path, $separator)->data());
    }

    /**
     * Get service
     * A bit caching
     * Can be overwritten in sub class, to set the correct return annotation
     *
     * @return Utils
     */
    protected function getService(): Utils
    {
        return $this->getServiceBase();
    }

}

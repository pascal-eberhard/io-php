<?php declare(strict_types=1);

/*
 * This file is part of the typed-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ConfigSimple\Tests\External\Io\Path\Strings;

use LogicException;
use PEPrograms\DataObject;
use PEPrograms\ConfigSimple\External\Io\Path\Strings\Data;
use PEPrograms\ConfigSimple\External\Io\Path\Strings\DataObjectInterface;
use PEPrograms\ConfigSimple\External\Io\Path\Strings\Value;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\ConfigSimple\External\Io\Path\Strings\Value
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/ValueTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class ValueTest extends TestCase
{

    /**
     * @return array, per item:
     * param string $exception Class path
     * param string $regex Exception message
     * param DataObjectInterface $data
     * param bool $emptyAllowed
     * @see self::testExceptions()
     */
    public function dataExceptions()
    {
        $return = [];

        // -
        $return[] = [
            LogicException::class,
            '/must not be empty/iu',
            (new Data()),
            false,
        ];

        // -
        $return[] = [
            LogicException::class,
            '/Path is empty, but path part list not/iu',
            (new Data())
                ->pathPartsAdd('x', true)
            ,
            true,
        ];

        // -
        $return[] = [
            LogicException::class,
            '/Path is empty, but separator list not/iu',
            (new Data())
                ->separatorsAdd('/', true)
            ,
            true,
        ];

        // -
        return $return;
    }

    /**
     * @covers ::__construct
     * @dataProvider dataExceptions
     *
     * @param string $exception Class path
     * @param string $regex Exception message
     * @param DataObjectInterface $data
     * @param bool $emptyAllowed
     *
     * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/ValueTest.php --filter '/::testExceptions\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testExceptions(string $exception, string $regex, DataObjectInterface $data, bool $emptyAllowed)
    {
        $this->expectException($exception);
        $this->expectExceptionMessageRegExp($regex);
        new Value($data, $emptyAllowed);
    }

    /**
     * @return array, per item:
     * param string[] $expected See ::toArray()
     * param Value $current
     * @see self::testFilled()
     * @see \PEPrograms\ConfigSimple\External\Io\Path\Strings\Value::toArray()
     */
    public function dataFilled()
    {
        $return = [];

        // -
        $return[] = [
            [
                'dataObject' => [
                    'path' => '/some/folder/file.txt',
                    'pathParts' => [
                        'some',
                        'file.txt',
                    ],
                    'pathPartsCount' => 2,
                    'separators' => [
                        '/',
                        '\\',
                    ],
                    'separatorsCount' => 2,
                ],
                'empty' => false,
            ],
            (new Data())
                ->pathSet('/some/folder/file.txt')
                ->pathPartsAdd('file.txt', false)
                ->pathPartsAdd('some', true)
                ->separatorsAdd('/', true)
                ->separatorsAdd('\\', false)

                ->toValueObject()
            ,
        ];

        // -
        return $return;
    }

    /**
     * Test empty data object
     *
     * @covers ::__construct
     * @covers ::__toString
     * @covers ::data
     * @covers ::empty
     *
     * @param Value|null $value Default:null
     * @return Value
     *
     * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/ValueTest.php --filter '/::testEmpty\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testEmpty(?Value $value = null): Value
    {
        // This way we are sure input and output data object are the same, if empty
        $dataTest = new DataTest();
        if (null === $value) {
            $value = new Value($dataTest->testEmpty());
        }

        $toArray = '. ' . DataObject\UnitTests\Utils::get()->buildToArrayMessageSuffix($value->data());

        $this->assertEquals(true, $value->empty(), '::empty()'. $toArray);
        $this->assertEquals('', '' . $value, '::__toString()'. $toArray);

        $expected = $value->toArray();
        $dataTest->testFilled($expected['dataObject'], $value->data());

        return $value;
    }

    /**
     * @covers ::__toString
     * @covers ::data
     * @covers ::empty
     * @dataProvider dataFilled
     *
     * @param string[] $expected See ::toArray()
     * @param Value $current
     *
     * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/ValueTest.php --filter '/::testFilled\b/')
     * @ \b, else all tests matching "testX*" would be executed
     * @see \PEPrograms\ConfigSimple\External\Io\Path\Strings\DataObjectInterface::toArray()
     */
    public function testFilled(array $expected, Value $current)
    {
        $toArray = '. ' . DataObject\UnitTests\Utils::get()->buildToArrayMessageSuffix($current->data());

        $this->assertEquals($expected['empty'], $current->empty(), '::empty()' . $toArray);
        $this->assertEquals($expected['dataObject']['path'], '' . $current, '::__toString()' . $toArray);

        (new DataTest())->testFilled($expected['dataObject'], $current->data());
    }
}

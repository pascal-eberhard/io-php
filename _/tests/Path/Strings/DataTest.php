<?php declare(strict_types=1);

/*
 * This file is part of the typed-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ConfigSimple\Tests\External\Io\Path\Strings;

use PEPrograms\DataObject;
use PEPrograms\ConfigSimple\External\Io\Path\Strings\Data;
use PEPrograms\ConfigSimple\External\Io\Path\Strings\DataObjectInterface;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\ConfigSimple\External\Io\Path\Strings\Data
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/DataTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class DataTest extends TestCase
{

    /**
     * @return array, per item:
     * param string[] $expected See ::toArray()
     * param DataObjectInterface $current
     * @see self::testFilled()
     * @see \PEPrograms\ConfigSimple\External\Io\Path\Strings\DataObjectInterface::toArray()
     */
    public function dataFilled()
    {
        $return = [];

        // -
        $return[] = [
            [
                'path' => '/some/folder/file.txt',
                'pathParts' => [
                    'some',
                    'file.txt',
                ],
                'pathPartsCount' => 2,
                'separators' => [
                    '/',
                    '\\',
                ],
                'separatorsCount' => 2,
            ],
            (new Data())
                ->pathSet('/some/folder/file.txt')
                ->pathPartsAdd('file.txt', false)
                ->pathPartsAdd('some', true)
                ->separatorsAdd('/', true)
                ->separatorsAdd('\\', false)
            ,
        ];

        // -
        return $return;
    }

    /**
     * Test empty data object
     *
     * @covers ::__construct
     * @covers ::__toString
     * @covers ::path
     * @covers ::pathParts
     * @covers ::pathPartsCount
     * @covers ::separators
     * @covers ::separatorsCount
     *
     * @param DataObjectInterface|null $data Default:null
     * @return DataObjectInterface
     *
     * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/DataTest.php --filter '/::testEmpty\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testEmpty(?DataObjectInterface $data = null): DataObjectInterface
    {
        if (null === $data) {
            $data = new Data();
        }

        $toArray = '. ' . DataObject\UnitTests\Utils::get()->buildToArrayMessageSuffix($data);

        // First small and easy data, e.g. array counts are more easy to check as whole arrays
        $this->assertEquals('', $data->path(), '::path()' . $toArray);
        $this->assertEquals('', '' . $data, '::__toString()' . $toArray);
        $this->assertEquals(0, $data->pathPartsCount(), '::pathPartsCount()' . $toArray);
        $this->assertEquals(0, $data->separatorsCount(), '::separatorsCount()' . $toArray);

        // Than the big data
        $this->assertEquals([], $data->pathParts(), '::pathParts()' . $toArray);
        $this->assertEquals([], $data->separators(), '::separators()' . $toArray);

        return $data;
    }

    /**
     * @covers ::__toString
     * @covers ::path
     * @covers ::pathParts
     * @covers ::pathPartsCount
     * @covers ::separators
     * @covers ::separatorsCount
     * @dataProvider dataFilled
     *
     * @param string[] $expected See ::toArray()
     * @param DataObjectInterface $current
     *
     * Shell: (vendor/bin/phpunit tests/External/Io/Path/Strings/DataTest.php --filter '/::testFilled\b/')
     * @ \b, else all tests matching "testX*" would be executed
     * @see \PEPrograms\ConfigSimple\External\Io\Path\Strings\DataObjectInterface::toArray()
     */
    public function testFilled(array $expected, DataObjectInterface $current)
    {
        $toArray = '. ' . DataObject\UnitTests\Utils::get()->buildToArrayMessageSuffix($current);

        // First small and easy data, e.g. array counts are more easy to check as whole arrays
        $this->assertEquals($expected['path'], '' . $current, '::__toString()' . $toArray);
        $this->assertEquals($expected['path'], $current->path(), '::path()' . $toArray);
        $this->assertEquals(
            $expected['pathPartsCount'],
            $current->pathPartsCount(),
            '::pathPartsCount()' . $toArray
        );
        $this->assertEquals(
            $expected['separatorsCount'],
            $current->separatorsCount(),
            '::separatorsCount()' . $toArray
        );

        // Than the big data
        $this->assertEquals($expected['pathParts'], $current->pathParts(), '::pathParts()' . $toArray);
        $this->assertEquals($expected['separators'], $current->separators(), '::separators()' . $toArray);
    }
}

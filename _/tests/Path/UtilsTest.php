<?php
declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Io\Tests\Path;

use PEPrograms\Io\Path\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PEPrograms\Io\Path\Utils
 *
 * Shell: (vendor/pascal-eberhard/project-tools-php/phpunit.phar tests/Path/UtilsTest.php)
 */
class UtilsTest extends TestCase
{

    /**
     * @see self::testDirname
     */
    public function dataDirname()
    {
        return [
            /**
             * @param string $result Converted path
             * @param string $path
             * @param int $level
             */
//            ['.', 'path', 1],
            ['..\\my', '..\\my\\path', 1],
//            ['C:\\', 'C:\\', 1],
//            ['C:\\', 'C:\\', 9],
//            ['C:\\', 'C:\\my\\path\\', 2],
//            ['C:\\', 'C:\\path', 1],
//            ['C:\\', 'C:\\path', 9],
//            ['C:\\my', 'C:\\my\\path', 1],
//            ['C:\\my', 'C:\\my\\path\\', 1],
        ];
    }

    /**
     * @covers ::dirname
     * @dataProvider dataDirname
     *
     * @param string $result Converted path
     * @param string $path
     * @param int $level
     * @see \dirname()
     *
     * Shell: (vendor/pascal-eberhard/project-tools-php/phpunit.phar tests/Path/UtilsTest.php --filter testDirname)
     */
    public function testDirname(string $result, string $path, int $level)
    {
        $this->assertEquals($result, Utils::dirname($path, $level));
    }
}

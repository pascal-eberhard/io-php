<?php declare(strict_types=1);

/*
 * This file is part of the typed-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ConfigSimple\Tests\Io\Path;

use LogicException;
use PEPrograms\ConfigSimple\Utils\OperatingSystemProperties;
use PEPrograms\ConfigSimple\UnitTests\WithServiceTrait;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\ConfigSimple\Utils\OperatingSystemProperties
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/Utils/OperatingSystemPropertiesTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class UtilsTest extends TestCase
{

    use WithServiceTrait;

    /**
     * Create service instance, different per sub class
     *
     * @return object
     */
    protected function createService()
    {
        return OperatingSystemProperties::get();
    }

    /**
     * Get service
     * A bit caching
     * Can be overwritten in sub class, to set the correct return annotation
     *
     * @return OperatingSystemProperties
     */
    protected function getService(): OperatingSystemProperties
    {
        return $this->getServiceBase();
    }

    /**
     * @covers ::detectFileSystemProperties
     *
     * Shell: (vendor/bin/phpunit tests/Utils/OperatingSystemPropertiesTest.php --filter '/::testX\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testX()
    {
        $this->assertTrue(true);
        print \PHP_EOL;
        \var_export($this->getService()->detectFileSystemProperties());
        print \PHP_EOL;
    }
}

<?php declare(strict_types=1);

/*
 * This file is part of the typed-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\ConfigSimple\Tests\Utils;

use LogicException;
use PEPrograms\ConfigSimple\Utils\Utils;
use PEPrograms\ConfigSimple\UnitTests\WithServiceTrait;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\ConfigSimple\Utils\Utils
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/Utils/UtilsTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class UtilsTest extends TestCase
{

    use WithServiceTrait;

    /**
     * Create service instance, different per sub class
     *
     * @return object
     */
    protected function createService()
    {
        return Utils::get();
    }

    /**
     * @return array, per item:
     * param string $result Expected
     * param string $path The full path
     * param string $suffix Path suffix, empty allowed
     * @see ServiceTest::testGetPathPrefix()
     */
    public function dataGetPathPrefix()
    {
        return [
            // Empty suffix
            ['/', '/', ''],

            // Suffix match
            ['/a/', '/a/b/c.txt', 'b/c.txt'],

            // Suffix not match by content
            ['', '/a/b/c.txt', 'c/d.jpg'],

            // Suffix not match by directory separator
            ['', '/a/b/c.txt', 'c\\d.jpg'],
        ];
    }

    /**
     * @return array, per item:
     * param string $exception Class path
     * param string $regex Exception message
     * param string $path The full path
     * param string $suffix Path suffix, empty allowed
     * @see ServiceTest::testGetPathPrefixExceptions()
     */
    public function dataGetPathPrefixExceptions()
    {
        return [
            [LogicException::class, '/path must not be empty/iu', '', ''],
            [LogicException::class, '/path must not be empty/iu', '', 'a'],
        ];
    }

    /**
     * @return array, per item:
     * param int $result Expected
     * param string $text
     * param bool $emptyAllowed
     * @see ServiceTest::testGetStringLength()
     */
    public function dataGetStringLength()
    {
        return [
            [0, '', true],
            [4, 'Käße', false],
            [4, 'Käße', true],
        ];
    }

    /**
     * @return array, per item:
     * param string $exception Class path
     * param string $regex Exception message
     * param string $text
     * param bool $emptyAllowed
     * @see ServiceTest::testGetStringLengthExceptions()
     */
    public function dataGetStringLengthExceptions()
    {
        return [
            [LogicException::class, '/text must not be empty/iu', '', false],
        ];
    }

    /**
     * Get service
     * A bit caching
     * Can be overwritten in sub class, to set the correct return annotation
     *
     * @return Utils
     */
    protected function getService(): Utils
    {
        return $this->getServiceBase();
    }
//
//    /**
//     * @covers ::detectProjectBaseDirectory
//     *
//     * Shell: (vendor/bin/phpunit tests/Utils/UtilsTest.php --filter testDetectProjectBaseDirectory)
//     */
//    public function testDetectProjectBaseDirectory()
//    {
//        $result = Service::get()->detectProjectBaseDirectory();
//        print \PHP_EOL;
//        \var_export($result->toArray());
//        print \PHP_EOL;
//        $this->assertTrue(true);
////        $result = $this->getService()->getOperatingSystemNameOrFamily();
////        $this->assertNotEmpty($result);
//    }

    /**
     * @covers ::getPathPrefix
     * @dataProvider dataGetPathPrefix
     *
     * @param string $result Expected
     * @param string $path The full path
     * @param string $suffix Path suffix, empty allowed
     *
     * Shell: (vendor/bin/phpunit tests/Utils/UtilsTest.php --filter '/::testGetPathPrefix\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testGetPathPrefix(string $result, string $path, string $suffix)
    {
        $this->assertEquals($result, $this->getService()->getPathPrefix($path, $suffix));
    }

    /**
     * @covers ::getPathPrefix
     * @dataProvider dataGetPathPrefixExceptions
     *
     * @param string $exception Class path
     * @param string $regex Exception message
     * @param string $path The full path
     * @param string $suffix Path suffix, empty allowed
     *
     * Shell: (vendor/bin/phpunit tests/Utils/UtilsTest.php --filter testGetPathPrefixExceptions)
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testGetPathPrefixExceptions(string $exception, string $regex, string $path, string $suffix)
    {
        $this->expectException($exception);
        $this->expectExceptionMessageRegExp($regex);
        $this->getService()->getPathPrefix($path, $suffix);
    }

    /**
     * @covers ::getStringLength
     * @dataProvider dataGetStringLength
     *
     * @param int $result Expected
     * @param string $text
     * @param bool $emptyAllowed
     *
     * Shell: (vendor/bin/phpunit tests/Utils/UtilsTest.php --filter '/::testGetStringLength\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testGetStringLength(int $result, string $text, bool $emptyAllowed)
    {
        $this->assertEquals($result, $this->getService()->getStringLength($text, $emptyAllowed));
    }

    /**
     * @covers ::getStringLength
     * @dataProvider dataGetStringLengthExceptions
     *
     * @param string $exception Class path
     * @param string $regex Exception message
     * @param string $text
     * @param bool $emptyAllowed
     *
     * Shell: (vendor/bin/phpunit tests/Utils/UtilsTest.php --filter testGetStringLengthExceptions)
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testGetStringLengthExceptions(string $exception, string $regex, string $text, bool $emptyAllowed)
    {
        $this->expectException($exception);
        $this->expectExceptionMessageRegExp($regex);
        $this->getService()->getStringLength($text, $emptyAllowed);
    }
////
////    /**
////     * @covers ::getOperatingSystemNameOrFamily
////     *
////     * Shell: (vendor/bin/phpunit tests/UtilsTest.php --filter testGetOperatingSystemNameOrFamily)
////     */
////    public function testGetOperatingSystemNameOrFamily()
////    {
////        print \PHP_EOL;
////        print '(' . \hash('sha1', '' . \microtime(true)) . ')';
////        print \PHP_EOL;
////        $this->assertTrue(true);
//////        $result = $this->getService()->getOperatingSystemNameOrFamily();
//////        $this->assertNotEmpty($result);
////    }
//
////    /**
////     * @return array Per item:
////     * * param bool $isValid Expected result
////     * * param mixed $value
////     * * param string $node
////     * @see ::testIsValid
////     */
////    public function dataIsValid(): array
////    {
////        return [
////            [false, null, ''],
////            [false, [], ''],
////            [true, false, ''],
////            [true, true, ''],
////        ];
////    }
////
////    /**
////     * @see self::testPathLinuxToWindows
////     */
////    public function dataPathLinuxToWindows()
////    {
////        return [
////            /**
////             * @param string $result Converted path
////             * @param string $path Must be absolute
////             *
////             * Hint: The drive letter is always converted to lower case
////             */
////            ['..\\mY\\Path', '../mY/Path'],
////            ['..\\mY\\Path\\', '../mY/Path/'],
////            ['C:\\windows', '/c/windows'],
////            ['C:\\Windows', '/c/Windows'],
////            ['C:\\windows\\', '/c/windows/'],
////            ['C:\\Windows\\', '/c/Windows/'],
////            ['mY\\Path', 'mY/Path'],
////            ['mY\\Path\\', 'mY/Path/'],
////            ['path', 'path'],
////        ];
////    }
////
////    /**
////     * @covers ::baseDir
////     *
////     * Shell: (vendor/bin/phpunit tests/MyTest.php --filter testDefault)
////     */
////    public function testDefault()
////    {
////        $this->assertTrue(true);
////        $type = new Typed\Type\IntType();
////
////        $type->validate(0);
////        $type->validate(true);
////    }
//////
//////    /**
//////     * @covers ::pathLinuxToWindows
//////     * @dataProvider dataPathLinuxToWindows
//////     *
//////     * @param string $result Converted path
//////     * @param string $path Must be absolute
//////     *
//////     * Shell: (vendor/bin/phpunit tests/ConfigTest.php --filter testPathLinuxToWindows)
//////     */
//////    public function testPathLinuxToWindows(string $result, string $path)
//////    {
//////        $this->assertEquals($result, Config::pathLinuxToWindows($path));
//////    }
//////
//////    /**
//////     * @covers ::pathWindowsToLinux
//////     * @dataProvider dataPathWindowsToLinux
//////     *
//////     * @param string $result Converted path
//////     * @param string $path Must be absolute
//////     *
//////     * Shell: (vendor/bin/phpunit tests/ConfigTest.php --filter testPathWindowsToLinux)
//////     */
//////    public function testPathWindowsToLinux(string $result, string $path)
//////    {
//////        $this->assertEquals($result, Config::pathWindowsToLinux($path));
//////    }
}

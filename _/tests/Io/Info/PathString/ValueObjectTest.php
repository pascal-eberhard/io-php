<?php

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\UnitTests\Io\Info\PathString;

use PascalEberhardProgramming\DataStore\Io\Info\PathString;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PascalEberhardProgramming\DataStore\Io\Info\PathString\Utils
 * Shell: (bin/phpunit test/unit/Io/Info/PathString/ValueObjectTest.php)
 */
class ValueObjectTest extends TestCase
{

    /**
     * @return array
     * @see self::testDefault
     */
    public function dataDefault(): array
    {
        $config = (new PathString\ConfigDO())
            ->setAbsolute(true)
            ->setAddDirectorySeparatorAtEnd(true)
            ->setWindowsToLinuxConvert(true)
        ;

        return [
            // string $out Expected output,
            // string $path,
            // \PascalEberhardProgramming\DataStore\Io\Info\PathString\ConfigDO $config

            ['/c/b/', 'C:\\b', $config],
        ];
    }

    /**
     * @return array
     * @see self::testExceptions
     */
    public function dataExceptions(): array
    {
        return [
            // string $class, string $messageRegex, string $path

            [\LogicException::class, '/Path must not be empty/iu', ''],
       ];
    }

    /**
     * @covers ::__construct
     * @dataProvider dataDefault
     *
     * @param string $out Expected output
     * @param string $path
     * @param \PascalEberhardProgramming\DataStore\Io\Info\PathString\ConfigDO $config
     * Shell: (bin/phpunit test/unit/Io/Info/PathString/ValueObjectTest.php --filter testDefault)
     */
    public function testDefault(string $out, string $path, PathString\ConfigDO $config)
    {
        $vo = new PathString\ValueObject($path, $config);
        $this->assertEquals($out, $vo->data());
        $this->assertEquals($path, $vo->input());
    }

    /**
     * @covers ::__construct
     * @dataProvider dataExceptions
     *
     * @param string $class
     * @param string $messageRegex
     * @param string $path
     * Shell: (bin/phpunit test/unit/Io/Info/PathString/ValueObjectTest.php --filter testExceptions)
     */
    public function testExceptions(string $class, string $messageRegex, string $path)
    {
        $this->expectException($class);
        $this->expectExceptionMessageRegExp($messageRegex);
        new PathString\ValueObject($path);
    }
}

<?php

/*
 * This file is part of the Data Store package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PascalEberhardProgramming\DataStore\UnitTests\FileInfo;

use PascalEberhardProgramming\DataStore\FileInfo\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @author Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PascalEberhardProgramming\DataStore\FileInfo\Utils
 * Shell: (bin/phpunit test/unit/FileInfo/UtilsTest.php)
 */
class UtilsTest extends TestCase
{

    /**
     * @return array
     * @see self::testParseFileHashDataLinux
     */
    public function dataParseFileHashDataLinux(): array
    {
        return [
            // string[] $out Expected output, string $data

            // Empty
            [[], ''],

            // Md5
            [
                [
                    [
                        'hash' => '160f134dbf9104ae6c499bbc27f70bff',
                        'path' => '/c/_buffer/zzz/markdown.html',
                    ],
                ], '160f134dbf9104ae6c499bbc27f70bff */c/_buffer/zzz/markdown.html'
            ],

            // Multiple data and file names with special chars
            [
                [
                    [
                        'hash' => '160f134dbf9104ae6c499bbc27f70bff',
                        'path' => '/c/_buffer/zzz/markdown - ! \'Copy.html',
                    ],
                    [
                        'hash' => '160f134dbf9104ae6c499bbc27f70bff',
                        'path' => '/c/_buffer/zzz/markdown.html',
                    ],
                    [
                        'hash' => 'ac793f5d8350be368c14cff23119ccecc01dadb7',
                        'path' => '/c/_buffer/zzz/markdown - ! \'Copy.html',
                    ],
                    [
                        'hash' => 'ac793f5d8350be368c14cff23119ccecc01dadb7',
                        'path' => '/c/_buffer/zzz/markdown.html',
                    ],
                ], '160f134dbf9104ae6c499bbc27f70bff */c/_buffer/zzz/markdown - ! \'Copy.html'
                . PHP_EOL . '160f134dbf9104ae6c499bbc27f70bff */c/_buffer/zzz/markdown.html'
                . PHP_EOL . 'ac793f5d8350be368c14cff23119ccecc01dadb7 */c/_buffer/zzz/markdown - ! \'Copy.html'
                . PHP_EOL . 'ac793f5d8350be368c14cff23119ccecc01dadb7 */c/_buffer/zzz/markdown.html'
            ],

            // Sha1
            [
                [
                    [
                        'hash' => 'ac793f5d8350be368c14cff23119ccecc01dadb7',
                        'path' => '/c/_buffer/zzz/markdown.html',
                    ],
                ], 'ac793f5d8350be368c14cff23119ccecc01dadb7 */c/_buffer/zzz/markdown.html'
            ],
        ];
    }

    /**
     * @return array
     * @see self::testParseFileSizeDataLinux
     */
    public function dataParseFileSizeDataLinux(): array
    {
        return [
            // string[] $out Expected output, string $data

            // Empty
            [[], ''],

            // One File
            [
                [
                    [
                        'path' => '/c/_buffer/zzz/markdown.html',
                        'size' => 52168,
                    ],
                ], '-rw-r--r-- 1 197121   52168 2018-09-01 23:05:32.105240700 +0200 /c/_buffer/zzz/markdown.html'
            ],

            // Multiple items
            [
                [
                    [
                        'path' => '/markdown.html',
                        'size' => 52168,
                    ],
                    [
                        'path' => '/markdown - ! \'Copy.html',
                        'size' => 327,
                    ],
                    [
                        'path' => '/folder',
                        'size' => 0,
                    ],
                    [
                        'path' => '/empty-file.txt',
                        'size' => 0,
                    ],
                    [
                        'path' => '/big-file.dat',
                        'size' => PHP_INT_MAX,
                    ],
                ], '-rw-r--r-- 1 197121   52168 2018-09-01 23:05:32.105240700 +0200 /markdown.html'
                . PHP_EOL . '-rw-r--r-- 1 197121   327 2018-09-01 23:05:32.105240700 +0200 /markdown - ! \'Copy.html'
                . PHP_EOL . 'drw-r--r-- 1 197121   0 2018-09-01 23:05:32.105240700 +0200 /folder'
                . PHP_EOL . '-rw-r--r-- 1 197121   0 2018-09-01 23:05:32.105240700 +0200 /empty-file.txt'
                . PHP_EOL . '-rw-r--r-- 1 197121   ' . PHP_INT_MAX . ' 2018-09-01 23:05:32.105200 +0200 /big-file.dat'
            ],
        ];
    }

    /**
     * @return array
     * @see self::testParsePathsLinux
     */
    public function dataParsePathsLinux(): array
    {
        return [
            // string[] $out Expected output, string $data

            // Amount: 0
            [[], ''],

            // Amount: 1
            [
                [
                    '/c/_buffer/zzz/markdown.html',
                ], '/c/_buffer/zzz/markdown.html'
            ],
            
            // Amount: > 1
            [
                [
                    '/c/_buffer/zzz/markdown.html',
                    '/c/_buffer/zzz/markdown - ! \'Copy.html',
                    '/c/_buffer/zzz/markdown-dir',
                    '/c/_buffer/zzz/markdown - ! \'Copy dir',
                ], '/c/_buffer/zzz/markdown.html'
                . PHP_EOL . '/c/_buffer/zzz/markdown - ! \'Copy.html'
                . PHP_EOL . '/c/_buffer/zzz/markdown-dir'
                . PHP_EOL . '/c/_buffer/zzz/markdown - ! \'Copy dir'
            ],
        ];
    }

    /**
     * @covers ::parseFileHashDataLinux
     * @dataProvider dataParseFileHashDataLinux
     * 
     * @param string[] $out Expected output
     * @param string $data
     * Shell: (bin/phpunit test/unit/FileInfo/UtilsTest.php --filter testParseFileHashDataLinux)
     */
    public function testParseFileHashDataLinux(array $out, string $data)
    {
//        // For debug
//        $this->assertTrue(true);
//        echo var_export([
//            'in' => $data,
//            'out' => Utils::parseFileHashDataLinux($data),
//        ], true);
        $this->assertEquals($out, Utils::parseFileHashDataLinux($data));
    }

    /**
     * @covers ::parseFileSizeDataLinux
     * @dataProvider dataParseFileSizeDataLinux
     *
     * @param string[] $out Expected output
     * @param string $data
     * Shell: (bin/phpunit test/unit/FileInfo/UtilsTest.php --filter testParseFileSizeDataLinux)
     */
    public function testParseFileSizeDataLinux(array $out, string $data)
    {
//        // For debug
//        $this->assertTrue(true);
//        echo var_export([
//            'in' => $data,
//            'out' => Utils::parseFileSizeDataLinux($data),
//        ], true);
        $this->assertEquals($out, Utils::parseFileSizeDataLinux($data));
    }

    /**
     * @covers ::parsePathsLinux
     * @dataProvider dataParsePathsLinux
     *
     * @param string[] $out Expected output
     * @param string $data
     * Shell: (bin/phpunit test/unit/FileInfo/UtilsTest.php --filter testParsePathsLinux)
     */
    public function testParsePathsLinux(array $out, string $data)
    {
//        // For debug
//        $this->assertTrue(true);
//        echo var_export([
//            'in' => $data,
//            'out' => Utils::parsePathsLinux($data),
//        ], true);
        $this->assertEquals($out, Utils::parsePathsLinux($data));
    }
}

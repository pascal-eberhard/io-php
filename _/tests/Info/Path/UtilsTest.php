<?php
declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\Io\Tests\Info\Path;

use PEPrograms\Io\Info\Path\Utils;
use PHPUnit\Framework\TestCase;

/**
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @coversDefaultClass \PEPrograms\Io\Info\Path\Utils
 *
 * Shell: (vendor/pascal-eberhard/project-tools-php/phpunit.phar tests/Info/Path/UtilsTest.php)
 */
class UtilsTest extends TestCase
{

    /**
     * @see self::testAddTailingSeparator
     */
    public function dataAddTailingSeparator()
    {
        return [
            /**
             * @param string $result Expected result
             * @param string $path
             * @param string $separator
             */
            ['/', '', '/'],
            ['/', '/', '/'],
            ['\\', '', '\\'],
            ['\\', '\\', '\\'],
            [DIRECTORY_SEPARATOR, '', DIRECTORY_SEPARATOR],
            [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR],
        ];
    }

    /**
     * @see self::testIsWindowsFormat
     */
    public function dataIsWindowsFormat()
    {
        return [
            /**
             * @param bool $result Expected flag
             * @param string $path
             */
            [false, '/'],
            [false, '\\'],
            [false, '/c/'],
            [false, '\\c:\\'],
            [true, 'c:\\'],
            [true, 'c:/'],
        ];
    }

    /**
     * @see self::testLinuxToWindows
     */
    public function dataLinuxToWindows()
    {
        return [
            /**
             * @param string $result Converted path
             * @param string $path
             *
             * Hint: The drive letter is always converted to upper case
             */
            ['..\\mY\\Path', '../mY/Path'],
            ['..\\mY\\Path\\', '../mY/Path/'],
            ['C:\\windows', '/c/windows'],
            ['C:\\Windows', '/c/Windows'],
            ['C:\\windows\\', '/c/windows/'],
            ['C:\\Windows\\', '/c/Windows/'],
            ['mY\\Path', 'mY/Path'],
            ['mY\\Path\\', 'mY/Path/'],
            ['path', 'path'],
        ];
    }

    /**
     * @see self::testSplit
     */
    public function dataSplit()
    {
        return [
            /**
             * @param string[] $result Expected parts
             * @param string $path
             */
//            [[], ''],
            [[], '/'],
        ];
    }
////
////    /**
////     * @see self::testPathSplit
////     */
////    public function dataPathSplit() {
////        return [
////            /**
////             * @param string[] $result Expected result
////             * @param string $path
////             */
//////            [[], ''],
//////            ['/', '/'],
////            [
////                [
////                    'C:',
////                    '\\',
////                    'projects',
////                    '\\',
////                    'my-project',
////                    '\\',
////                    'vendor',
////                    '\\',
////                    'package',
////                    '\\',
////                    'vendor',
////                    '\\',
////                    'file.ext',
////                ], 'C:\\projects\\my-project\\vendor\\package\\vendor\\file.ext'
////            ],
////        ];
////    }

    /**
     * @see self::testWindowsToLinux
     */
    public function dataWindowsToLinux()
    {
        return [
            /**
             * @param string $result Converted path
             * @param string $path
             *
             * Hint: The drive letter is always converted to lower case
             */
            ['../mY/Path', '..\\mY\\Path'],
            ['../mY/Path/', '..\\mY\\Path\\'],
            ['/c/windows', 'C:\\windows'],
            ['/c/Windows', 'c:\\Windows'],
            ['/c/windows/', 'C:\\windows\\'],
            ['/c/Windows/', 'c:\\Windows\\'],
            ['mY/Path', 'mY\\Path'],
            ['mY/Path/', 'mY\\Path\\'],
            ['path', 'path'],
        ];
    }

    /**
     * @covers ::addTailingSeparator
     * @dataProvider dataAddTailingSeparator
     *
     * @param string $result Expected result
     * @param string $path
     * @param string $separator
     * Shell: (vendor/pascal-eberhard/project-tools-php/phpunit.phar tests/Info/Path/UtilsTest.php --filter testAddTailingSeparator)
     */
    public function testAddTailingSeparator(string $result, string $path, string $separator)
    {
        $this->assertEquals($result, Utils::addTailingSeparator($path, $separator));
    }

    /**
     * @covers ::isWindowsFormat
     * @dataProvider dataIsWindowsFormat
     *
     * @param bool $result Expected flag
     * @param string $path
     * Shell: (vendor/pascal-eberhard/project-tools-php/phpunit.phar tests/Info/Path/UtilsTest.php --filter testIsWindowsFormat)
     */
    public function testIsWindowsFormat(bool $result, string $path)
    {
        $this->assertEquals($result, Utils::isWindowsFormat($path));
    }

    /**
     * @covers ::linuxToWindows
     * @dataProvider dataLinuxToWindows
     *
     * @param string $result Converted path
     * @param string $path
     *
     * Shell: (vendor/pascal-eberhard/project-tools-php/phpunit.phar tests/Info/Path/UtilsTest.php --filter testLinuxToWindows)
     */
    public function testLinuxToWindows(string $result, string $path)
    {
        $this->assertEquals($result, Utils::linuxToWindows($path));
    }

    /**
     * @covers ::split
     * @dataProvider dataSplit
     *
     * @param string[] $result Expected parts
     * @param string $path
     *
     * Shell: (vendor/pascal-eberhard/project-tools-php/phpunit.phar tests/Info/Path/UtilsTest.php --filter testSplit)
     */
    public function testSplit(array $result, string $path)
    {
        $this->assertEquals($result, Utils::split($path));
    }

    /**
     * @covers ::windowsToLinux
     * @dataProvider dataWindowsToLinux
     *
     * @param string $result Converted path
     * @param string $path
     *
     * Shell: (vendor/pascal-eberhard/project-tools-php/phpunit.phar tests/Info/Path/UtilsTest.php --filter testWindowsToLinux)
     */
    public function testWindowsToLinux(string $result, string $path)
    {
        $this->assertEquals($result, Utils::windowsToLinux($path));
    }
}

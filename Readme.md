# I/O tools

## Description

Some I/O and path tools.

### [Change log](resources/docs/changelog.md)

### License

The MIT License, see [License File](LICENSE.md).

## Installation

Via composer:

```bash
composer require pascal-eberhard/io-php
```

## Some shell commands

```bash
# All checks
composer checks

# One at a time, see composer.json "scripts"
#composer [insert composer script label, without @ prefix]
```

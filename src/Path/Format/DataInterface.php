<?php declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Io\Path\Format;

use PEPrograms\DataObject;
use PEPrograms\Utils\ClassAndObject\With;

/**
 * Data object for I/O path format
 *
 * I/O Path format are only the I/O path data string format, independent from the file system and real I/O Paths
 * I/O Path in this package are based at the I/O Path format data
 * But I/O Path are for real I/O actions, like check if exist or list contained files and folders
 * I/O Path format is only the data basis
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see \PEPrograms\Io\Path
 */
interface DataInterface extends
    DataObject\TheInterface,
    With\ToArray\TheInterface,
    With\ToString\TheInterface
    /* , DataObject\With\ToValueObjectInterface*/
{

    /**
     * Get full path
     *
     * @return string
     */
    public function path(): string;

    /**
     * Set full path
     *
     * @param string $path
     * @return $this
     */
    public function pathSet(string $path);

    /**
     * Get path parts
     *
     * @return string[]
     */
    public function pathParts(): array;

    /**
     * Add path part
     *
     * @param string $path
     * @param bool $atStart TRUE: at start -, FALSE at end of the list
     * @return $this
     */
    public function pathPartsAdd(string $path, bool $atStart);

    /**
     * Get path parts count
     *
     * @return int
     */
    public function pathPartsCount(): int;

    /**
     * Starts with path part or separator?
     *
     * @return bool
     */
    public function pathPartsFirst(): bool;

    /**
     * Get separator list
     *
     * @return string[]
     */
    public function separators(): array;

    /**
     * Add separator to list
     *
     * @param string $separator Must not be empty
     * @param bool $atStart TRUE: at start -, FALSE at end of the list
     * @return $this
     */
    public function separatorsAdd(string $separator, bool $atStart);

    /**
     * Get separator list count
     *
     * @return int
     */
    public function separatorsCount(): int;
}

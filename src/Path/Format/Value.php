<?php declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Io\Path\Format;

use PEPrograms\Utils\ClassAndObject\With;
use PEPrograms\ValueObject;
use PEPrograms\ValueObject\Simple\Strings;

/**
 * Value object for I/O path format data object
 *
 * I/O Path format are only the I/O path data string format, independent from the file system and real I/O Paths
 * I/O Path in this package are based at the I/O Path format data
 * But I/O Path are for real I/O actions, like check if exist or list contained files and folders
 * I/O Path format is only the data basis
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see \PEPrograms\Io\Path
 */
final class Value implements With\ToArray\TheInterface, With\ToString\TheInterface, ValueObject\TheInterface
{

    /**
     * @var DataInterface|null
     */
    private $data = null;

    /**
     * @var bool
     */
    private $empty = true;

    /**
     * Constructor
     *
     * @param DataInterface $data
     * @param bool $emptyAllowed Default:TRUE
     * @throws \LogicException
     */
    public function __construct(DataInterface $data, bool $emptyAllowed = true)
    {
        $this->empty = Strings\Factory::get()->create($data->path(), $emptyAllowed)->empty();
        if ($this->empty && $data->pathPartsCount() > 0) {
            throw new \LogicException('Inconsistent data. Path string is empty, but path part list not. '
                . 'List (var exported): ' . \var_export($data->pathParts(), true));
        } elseif ($this->empty && $data->separatorsCount() > 0) {
            throw new \LogicException('Inconsistent data. Path string is empty, but separator list not. '
                . 'List (var exported): ' . \var_export($data->separators(), true));
        } elseif (!$this->empty && $data->separatorsCount() < 1 && $data->pathPartsCount() < 1) {
            throw new \LogicException('Inconsistent data. Path string is NOT empty, but path parts and '
                . 'separator list. Path string (var exported): ' . \var_export($data->path(), true));
        } elseif (!$this->empty && $data->separatorsCount() < 1 && $data->pathPartsCount() > 1) {
            $errorData = [
                'path' => $data->path(),
                'pathParts' => $data->pathParts(),
            ];
            throw new \LogicException('Inconsistent data. Separator list is empty, but more than one Path '
                . 'parts (var exported): ' . \var_export($errorData, true));
        }

        $this->data = $data;
    }

    /**
     * To string
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->data->toString();
    }

    /**
     * Get data object
     *
     * @return DataInterface
     */
    public function data(): DataInterface
    {
        // @todo Make generic

        // A value object should be read only after construction
        // You can't change returned scalar attributes from outside the object
        // But you can change returned object attributes from outside the object
        // Therefore only return cloned object attributes

        return clone $this->data;
    }

    /**
     * Is empty
     *
     * @return bool
     */
    public function empty(): bool
    {
        return $this->empty;
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'data' => $this->data->toArray(),
            'empty' => $this->empty,
        ];
    }

    /**
     * To string
     *
     * @return string
     */
    public function toString(): string
    {
        return $this->__toString();
    }
}

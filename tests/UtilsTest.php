<?php declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Io\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \PEPrograms\Io\Utils
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * Shell: (vendor/bin/phpunit tests/UtilsTest.php)
 * @ \b, else all tests matching "testX*" would be executed
 */
class UtilsTest extends TestCase
{
    /**
     * @covers ::doSomething
     * Shell: (vendor/bin/phpunit tests/UtilsTest.php --filter '/::testDoSomething\b/')
     * @ \b, else all tests matching "testX*" would be executed
     */
    public function testDoSomething()
    {
        $this->assertTrue(true);
    }
}

<?php declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Io\Path\Format;

use PEPrograms\Utils\ClassAndObject\With\GetInstanceSelf;
use PEPrograms\ValueObject\Simple\Strings;

/**
 * Some I/O path format tools
 * Based at Symfony service concept
 *
 * I/O Path format are only the I/O path data string format, independent from the file system and real I/O Paths
 * I/O Path in this package are based at the I/O Path format data
 * But I/O Path are for real I/O actions, like check if exist or list contained files and folders
 * I/O Path format is only the data basis
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see \PEPrograms\Io\Path
 */
final class Utils implements GetInstanceSelf\TheInterface
{

    /**
     * @return self
     */
    public static function get()
    {
        return new self();
    }

    /**
     * Combine path parts back to string
     *
     * @param Value $valueObject Empty allowed
     * @param string $separator Directory separator, default: DIRECTORY_SEPARATOR
     * @return string
     * @see \DIRECTORY_SEPARATOR
     */
    public function combine(Value $valueObject, string $separator = ''): string
    {
        // Empty?
        if ($valueObject->empty()) {
            return '';
        }

        // Only one path part?
        $data = $valueObject->data();
        if ($data->separatorsCount() < 1) {
            $result = $data->pathParts();

            return $result[1];
        }

        // Re-combine
        $isPathPart = $data->pathPartsFirst();
        $path = '';
        $pathParts = $data->pathParts();
        $separatorList = $data->separators();
        while (!empty($pathParts) && !empty($separatorList)) {
            if ($isPathPart && !empty($pathParts)) {
                $path .= \array_shift($pathParts);
            } elseif (!$isPathPart && !empty($separatorList)) {
                $path .= \array_shift($separatorList);
            }
            $isPathPart = !$separatorList;
        }

        return $path;
    }

    /**
     * Split an I/O path string
     *
     * @param string $path Empty allowed
     * @param string $separator Directory separator, default: DIRECTORY_SEPARATOR
     * @param string $charset Optional
     * @return Value
     * @throws \LogicException
     * @throws \UnexpectedValueException
     * @todo $separator as array
     * @codingStandardsIgnoreStart
     * @todo CHECK different directory separators <https://en.wikipedia.org/w/index.php?title=Path_(computing)&oldid=956140132#Representations_of_paths_by_operating_system_and_shell>
     * @codingStandardsIgnoreEnd
     *
     * @see \DIRECTORY_SEPARATOR
     */
    public function split(string $path, string $separator = '', string $charset = ''): Value
    {
        $path = new Strings\Value($path, true, $charset);
        $charset = $path->charset();

        // Empty path?
        if ($path->empty()) {
            return (new Data())
                ->toValueObject();
        }

        // Split by separator
        $separator = new Strings\Value($separator, false, $charset, \DIRECTORY_SEPARATOR);

        // The actually part splitting
        return $this->splitTheParts($path, $separator);
    }

    /**
     * Split an I/O path, the actually part splitting
     *
     * @param Strings\Value $path
     * @param Strings\Value $separator
     * @return Value
     * @throws \LogicException
     * @throws \UnexpectedValueException
     */
    protected function splitTheParts(Strings\Value $path, Strings\Value $separator): Value
    {
        $path->checkEmptyOrNot(false);
        $separator->checkEmptyOrNot(false);

        $charset = $path->charset();

        // Split from the begin of the path
        $data = (new Data())->pathSet('' . $path);
        do {
            $nextSeparatorPos = \mb_strpos('' . $path, '' . $separator, 0, $charset);
            if (false === $nextSeparatorPos) {
                if (!$path->empty()) {
                    $data->pathPartsAdd('' . $path, false);
                }
                break;
            }

            if ($nextSeparatorPos < 1) {
                $chunk = new Strings\Value(
                    \mb_substr('' . $path, 0, $nextSeparatorPos + $separator->length(), $charset),
                    false,
                    $charset
                );
                $data->separatorsAdd('' . $separator, false);
                $path = new Strings\Value(
                    \mb_substr('' . $path, $chunk->length(), null, $charset),
                    true,
                    $charset
                );
            } else {
                $chunk = new Strings\Value(
                    \mb_substr('' . $path, 0, $nextSeparatorPos, $charset),
                    false,
                    $charset
                );
                $data->pathPartsAdd('' . $chunk, false);
                $path = new Strings\Value(
                    \mb_substr('' . $path, $nextSeparatorPos, null, $charset),
                    true,
                    $charset
                );
            }
        } while (false !== $nextSeparatorPos);

        return $data->toValueObject();
    }
}

<?php declare(strict_types=1);

/*
 * This file is part of the io-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE.md
 */

namespace PEPrograms\Io\Path\Format;

use PEPrograms\ValueObject\Simple\Strings;

/**
 * Data object for I/O path format
 *
 * I/O Path format are only the I/O path data string format, independent from the file system and real I/O Paths
 * I/O Path in this package are based at the I/O Path format data
 * But I/O Path are for real I/O actions, like check if exist or list contained files and folders
 * I/O Path format is only the data basis
 *
 * @copyright 2020 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 * @see \PEPrograms\Io\Path
 */
final class Data implements DataInterface
{

    /**
     * To string
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->path;
    }

    /**
     * @var string
     */
    private $path = '';

    /**
     * Get full path
     *
     * @return string
     */
    public function path(): string
    {
        return $this->path;
    }

    /**
     * Set full path
     *
     * @param string $path
     * @return $this
     */
    public function pathSet(string $path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @var string[]
     */
    private $pathParts = [];

    /**
     * Get path parts
     *
     * @return string[]
     */
    public function pathParts(): array
    {
        return $this->pathParts;
    }

    /**
     * Add path part
     *
     * @param string $path Must not be empty
     * @param bool $atStart TRUE: at start -, FALSE at end of the list, default: TRUE
     * @return $this
     */
    public function pathPartsAdd(string $path, bool $atStart = true)
    {
        $path = '' . Strings\Factory::get()->create($path, false);
        if ($this->pathPartsCount < 1 && $this->separatorsCount < 1) {
            $this->pathPartsFirst = true;
        }

        if ($atStart) {
            // @todo Array Utils with error check
            \array_unshift($this->pathParts, '' . $path);
        } else {
            \array_push($this->pathParts, '' . $path);
        }
        ++$this->pathPartsCount;

        return $this;
    }

    /**
     * @var int
     */
    private $pathPartsCount = 0;

    /**
     * Get path parts count
     *
     * @return int
     */
    public function pathPartsCount(): int
    {
        return $this->pathPartsCount;
    }

    /**
     * @var bool
     */
    private $pathPartsFirst = false;

    /**
     * Starts with path part or separator?
     *
     * @return bool
     */
    public function pathPartsFirst(): bool
    {
        return $this->pathPartsFirst;
    }

    /**
     * @var string[]
     */
    private $separators = [];

    /**
     * Get separator list
     *
     * @return string[]
     */
    public function separators(): array
    {
        return $this->separators;
    }

    /**
     * Add separator to list
     *
     * @param string $separator Must not be empty
     * @param bool $atStart TRUE: at start -, FALSE at end of the list
     * @return $this
     */
    public function separatorsAdd(string $separator, bool $atStart)
    {
        $separator = '' . Strings\Factory::get()->create($separator, false);
        if ($this->pathPartsCount < 1 && $this->separatorsCount < 1) {
            $this->pathPartsFirst = false;
        }

        if ($atStart) {
            // @todo Array Utils with error check
            \array_unshift($this->separators, $separator);
        } else {
            \array_push($this->separators, $separator);
        }
        ++$this->separatorsCount;

        return $this;
    }

    /**
     * @var int
     */
    private $separatorsCount = 0;

    /**
     * Get separator list count
     *
     * @return int
     */
    public function separatorsCount(): int
    {
        return $this->separatorsCount;
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'path' => $this->path,
            'pathParts' => $this->pathParts,
            'pathPartsCount' => $this->pathPartsCount,
            'separators' => $this->separators,
            'separatorsCount' => $this->separatorsCount,
        ];
    }

    /**
     * To string
     *
     * @return string
     */
    public function toString(): string
    {
        return $this->path;
    }

    /**
     * To related value object
     *
     * @return Value
     */
    public function toValueObject(): Value
    {
        return new Value($this);
    }
}
